import { FinalProjectBookPage } from './app.po';

describe('final-project-book App', () => {
  let page: FinalProjectBookPage;

  beforeEach(() => {
    page = new FinalProjectBookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
