import { ContactModel } from './../modules/contact-model';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ContactDatabaseService {
  myContacts: ContactModel[] = [];
  sendContact = new EventEmitter();

  constructor() { }

  setContact(newContact: ContactModel): void {
    this.myContacts.push(newContact);
    this.sendContact.emit(this.myContacts);
  }

  getContact(id: number): ContactModel {
    let contactModel: ContactModel;
    contactModel = this.myContacts[id]; 
    return contactModel;
  }
}
